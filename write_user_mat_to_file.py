import os,numpy,pickle,csv,itertools
def write_user_trust_matrix_to_file(outfile, inputfile):
	with open(outfile,'w') as outputfile:
		writer=csv.writer(outputfile)
		row_dict={-1:[-1]}
		with open(inputfile, 'r') as inpfile:
			for line in inpfile.readlines():
				temp_list=line.split(',')
				row=int(temp_list[0].split(':')[0])
				temp_list[0]=temp_list[0].split(":")[1]
				temp_list=list(map((lambda x: int(x)),temp_list))
				row_dict[row]=temp_list
		row_dict[1000]=[-1]	
		row_list=sorted(row_dict.iterkeys())
		for i in range(len(row_list)-1):
			for j in range(row_list[i]+1,row_list[i+1]):
				writer.writerow(numpy.zeros(10000))
			temp_array=numpy.zeros(10000)
			for k in row_dict[row_list[i+1]]:
				temp_array[k]=1
			writer.writerow(temp_array)


def get_user_trust_mat_row(outfile, row):
	if row > 100000:
		return -1
	with open(outfile, 'r') as inp:
		line = next(itertools.islice(csv.reader(inp), row, None))
		#print type(line[0])
		return list(map((lambda x: int(float(x))),line))


def main():
	write_user_trust_matrix_to_file("output.csv","input.txt")
	print "val  = \n"
	print get_user_trust_mat_row("output.csv",12)
if __name__=="__main__":
	main()