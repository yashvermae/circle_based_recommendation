#   U.csv is small instance of User_Trust_Network.csv 
#   Ra.csv is small instance of Rating.csv
import pandas as pd
import re
import numpy as np

d=10

Dict_Product_Category={}
No_of_items_in_category={}
TRAINING_MATRICES={}
Category_Circle={}
Category_to_List_of_items={}
def Trustee_List(Trustee):
	Trustee_set=set()
	for trustee in Trustee.split(","):
		Trustee_set.add(int(trustee))
	return Trustee_set

def Load_Dictionary(Product_df):
	for i in range(len(Product_df)):
		product_id=Product_df['Product_id'][i]
		Category_name=Product_df['Category_names'][i]
		Dict_Product_Category[product_id]=Category_name
		if Category_name not in No_of_items_in_category:
			No_of_items_in_category[Category_name]=0
		if Category_name not in Category_to_List_of_items:
			Category_to_List_of_items[Category_name]=list()
		No_of_items_in_category[Category_name]+=1
		Category_to_List_of_items[Category_name].append(product_id)

Product_df=pd.read_csv("Products.csv") 
# Product_df=pd.read_csv("Pr.csv") 

Load_Dictionary(Product_df)

# User_Trust_Network_df=pd.read_csv("User_Trust_Network.csv", sep=':') 
User_Trust_Network_df=pd.read_csv("U.csv", sep=':') 
User_Trust_Network_df['Trustee'] = map(Trustee_List,User_Trust_Network_df['Trustee'])
# print User_Trust_Network_df['Trustee'][0]
items=len(Product_df)
# users=len(User_Trust_Network_df)

# Training_df=pd.read_csv("Rating.csv")
Training_df=pd.read_csv("Ra.csv")
List_Users=list(set(Training_df['User_id']))
users=len(List_Users)
print users

with open("Category.txt") as Category_file:
# with open("Ca.txt") as Category_file:
	for Category in Category_file:
		if Category.strip() in No_of_items_in_category:
			
			P=np.random.randn(No_of_items_in_category[Category.strip()], d)
			P = P.astype(dtype = np.float16)

			Q=np.random.randn(users, d)
			Q=Q.astype(dtype = np.float16)

			R=np.empty([users, No_of_items_in_category[Category.strip()]], dtype = np.float16)
			
			S=np.empty([users,users], dtype = np.float16)
			S.fill(0.0)

			r=np.empty([users,No_of_items_in_category[Category.strip()]], dtype = np.float16)       
			
			TRAINING_MATRICES[Category.strip()]=[P,Q,R,S,r]

 
List_Category_row=[]
for i in range(len(Training_df)):
	List_Category_row.append(Dict_Product_Category[Training_df['Product_id'][i]])
Training_df['Category']=List_Category_row
# print Training_df['Category']


# Filling matrix r
r_values={}
for category,rating_group in Training_df.groupby('Category'):
	r_values[category]= np.mean(rating_group['Rating'])

for category in r_values:
	r=TRAINING_MATRICES[category][4]
	r.fill(r_values[category])


# Filling matrix R
for user,item,category,rating in zip(Training_df['User_id'],Training_df['Product_id'],Training_df['Category'],Training_df['Rating']):
	if category in TRAINING_MATRICES:
		R=TRAINING_MATRICES[category][2]
		row_index=List_Users.index(user)
		col_index=Category_to_List_of_items[category].index(item)
		R[row_index][col_index]=rating

# Filling Matrix S as Category_Circle (Varient a)
for user, rating_group in Training_df.groupby('User_id'):
	if user in List_Users:
	    for rating_entry in rating_group['Product_id']:
	    	if rating_entry in Dict_Product_Category:
		    	if Dict_Product_Category[rating_entry] not in Category_Circle:
		    		Category_Circle[Dict_Product_Category[rating_entry]]=dict()
		    	if user not in Category_Circle[Dict_Product_Category[rating_entry]]:
		    		Category_Circle[Dict_Product_Category[rating_entry]][user]=float(0)
		    	Category_Circle[Dict_Product_Category[rating_entry]][user]+=1

List_Category=[]
for Category in Category_Circle:
	List_Category.append(Category)

c=0
for Category in List_Category:
	if Category in TRAINING_MATRICES:
		user_list=set(Category_Circle[Category].keys())
		c+=1
		# print "Category  : ",Category,c
		for i in range(users):
			user=User_Trust_Network_df['User_id'][i]
			if user in List_Users:
				trustee=User_Trust_Network_df['Trustee'][i]
				Same_Category_trustee = trustee.intersection(user_list)
				if user in user_list:
					Same_Category_trustee.add(user)
				S=TRAINING_MATRICES[Category][3]
				for trustee_user in Same_Category_trustee:
					# print "hello"
					row_index=List_Users.index(user)
					col_index=List_Users.index(trustee_user)
					# print "row_index : ",row_index,"col_index : ",col_index
					# print "cnt : ",Category_Circle[Category][trustee_user]
					S[row_index][col_index]=Category_Circle[Category][trustee_user]

print "********************************"

#Normalize Each row of matrix S
c=0
for Category in TRAINING_MATRICES.keys():
	c+=1
	print "Category  : ",Category,c
	S=TRAINING_MATRICES[Category][3]
	for i in range(users):
		total_row_WT=0.0
		for j in range(users):
			total_row_WT+=S[i][j]
		if total_row_WT!=0:
			for j in range(users):
				S[i][j]/=total_row_WT

	# fname="./S_mat/S_matrix_for_"+str(c)
	# fd=open(fname,"w")
	# for i in range(users):
	# 	for j in range(users):
	# 		fd.write(str(S[i][j])+" ")	
	# 	fd.write("\n")	
	# fd.close()