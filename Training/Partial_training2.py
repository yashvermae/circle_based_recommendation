import pandas as pd
import re
import numpy as np

d=10

Dict_Product_Category={}
No_of_items_in_category={}
TRAINING_MATRICES={}
Category_Circle={}
Category_to_List_of_items={}

def Trustee_List(Trustee):
	Trustee_set=set()
	for trustee in Trustee.split(","):
		Trustee_set.add(int(trustee))
	return Trustee_set

def Load_Dictionary(Product_df):
	for i in range(len(Product_df)):
		product_id=Product_df['Product_id'][i]
		Category_name=Product_df['Category_names'][i]
		Dict_Product_Category[product_id]=Category_name
		if Category_name not in No_of_items_in_category:
			No_of_items_in_category[Category_name]=0
		if Category_name not in Category_to_List_of_items:
			Category_to_List_of_items[Category_name]=list()
		No_of_items_in_category[Category_name]+=1
		Category_to_List_of_items[Category_name].append(product_id)

Product_df=pd.read_csv("Products.csv") 
# Product_df=pd.read_csv("Pr.csv") 

Load_Dictionary(Product_df)

# User_Trust_Network_df=pd.read_csv("User_Trust_Network.csv", sep=':') 
User_Trust_Network_df=pd.read_csv("U.csv", sep=':') 
User_Trust_Network_df['Trustee'] = map(Trustee_List,User_Trust_Network_df['Trustee'])
# print User_Trust_Network_df['Trustee'][0]
items=len(Product_df)
# users=len(User_Trust_Network_df)

# Training_df=pd.read_csv("Rating.csv")
Training_df=pd.read_csv("Ra.csv")
List_Users=list(set(Training_df['User_id']))
users=len(List_Users)
print users

np.random.seed(0) 
with open("Category.txt") as Category_file:
# with open("Ca.txt") as Category_file:
	for Category in Category_file:
		if Category.strip() in No_of_items_in_category:
			
			P=np.random.randn(No_of_items_in_category[Category.strip()], d)
			P = P.astype(dtype = np.float16)

			Q=np.random.randn(users, d)
			Q=Q.astype(dtype = np.float16)

			R=np.empty([users, No_of_items_in_category[Category.strip()]], dtype = np.float16)
			R.fill(0.0)

			S=np.empty([users,users], dtype = np.float16)
			S.fill(0.0)

			r=np.empty([users,No_of_items_in_category[Category.strip()]], dtype = np.float16)       
			r.fill(0.0)

			I=np.empty([users, No_of_items_in_category[Category.strip()]], dtype = np.float16)
			I.fill(0.0)

			TRAINING_MATRICES[Category.strip()]=[P,Q,R,S,r,I]

 
List_Category_row=[]
for i in range(len(Training_df)):
	List_Category_row.append(Dict_Product_Category[Training_df['Product_id'][i]])
Training_df['Category']=List_Category_row
# print Training_df['Category']


# Filling matrix r
Rating_count={}
r_values={}
for category,rating_group in Training_df.groupby('Category'):
	r_values[category]= np.mean(rating_group['Rating'])
	Rating_count[category]=len(rating_group['Rating'])

for category in r_values:
	r=TRAINING_MATRICES[category][4]
	r.fill(r_values[category])


# Filling matrix R
for user,item,category,rating in zip(Training_df['User_id'],Training_df['Product_id'],Training_df['Category'],Training_df['Rating']):
	if category in TRAINING_MATRICES:
		R=TRAINING_MATRICES[category][2]
		I=TRAINING_MATRICES[category][5]
		row_index=List_Users.index(user)
		col_index=Category_to_List_of_items[category].index(item)
		R[row_index][col_index]=rating
		I[row_index][col_index]=1
# Filling Matrix S as Category_Circle (Varient a)
for user, rating_group in Training_df.groupby('User_id'):
	if user in List_Users:
	    for rating_entry in rating_group['Product_id']:
	    	if rating_entry in Dict_Product_Category:
		    	if Dict_Product_Category[rating_entry] not in Category_Circle:
		    		Category_Circle[Dict_Product_Category[rating_entry]]=dict()
		    	if user not in Category_Circle[Dict_Product_Category[rating_entry]]:
		    		Category_Circle[Dict_Product_Category[rating_entry]][user]=float(0)
		    	Category_Circle[Dict_Product_Category[rating_entry]][user]+=1

List_Category=[]
for Category in Category_Circle:
	List_Category.append(Category)

c=0
for Category in List_Category:
	if Category in TRAINING_MATRICES:
		user_list=set(Category_Circle[Category].keys())
		c+=1
		# print "Category  : ",Category,c
		for i in range(users):
			user=User_Trust_Network_df['User_id'][i]
			if user in List_Users:
				trustee=User_Trust_Network_df['Trustee'][i]
				Same_Category_trustee = trustee.intersection(user_list)
				if user in user_list:
					Same_Category_trustee.add(user)
				S=TRAINING_MATRICES[Category][3]
				for trustee_user in Same_Category_trustee:
					# print "hello"
					row_index=List_Users.index(user)
					col_index=List_Users.index(trustee_user)
					# print "row_index : ",row_index,"col_index : ",col_index
					# print "cnt : ",Category_Circle[Category][trustee_user]
					S[row_index][col_index]=Category_Circle[Category][trustee_user]

print "********************************"

#Normalize Each row of matrix S
c=0
for Category in TRAINING_MATRICES.keys():
	c+=1
	# print "Category  : ",Category,c
	S=TRAINING_MATRICES[Category][3]
	for i in range(users):
		total_row_WT=0.0
		for j in range(users):
			total_row_WT+=S[i][j]
		if total_row_WT!=0:
			for j in range(users):
				S[i][j]/=total_row_WT

	# fname="./S_mat/S_matrix_for_"+str(c)
	# fd=open(fname,"w")
	# for i in range(users):
	# 	for j in range(users):
	# 		fd.write(str(S[i][j])+" ")	
	# 	fd.write("\n")	
	# fd.close()

print "********************************"

lamda = 0.1      # regularization constant
#change this 
l = 0.0001       # learning rate
#change this
beta = 20        # Social information weight  

# Error derivative wrt Q
def Error_derivative_wrt_Q(I, R_Predicted,R, P,Q, S):
    First_term = np.matmul(np.multiply(I,(R_Predicted-R)), P)
    Second_term = lamda * Q
    Third_term = beta * np.subtract(Q, np.matmul(S,Q))
    fourth_term = beta *  np.matmul(S.transpose(),np.subtract(Q, np.matmul(S,Q))) 
    return First_term + Second_term + Third_term - fourth_term 

# Error derivative wrt P
def Error_derivative_wrt_P(I,R_Predicted,R,P,Q):
    First_term = np.matmul(np.multiply(I,(R_Predicted-R)).transpose(), Q)
    Second_term = lamda * P
    return First_term + Second_term 

# Training 
def Training(I,R_Predicted,R,P,Q,S):
    First_term = np.sum(np.sum(np.multiply(R - np.multiply(R_Predicted,I), R - np.multiply(R_Predicted,I)))) / (2.0)
    Second_term = (lamda/(2.0)) *(np.linalg.norm(Q,ord='fro') + np.linalg.norm(P,ord='fro'))
    Third_term_temp = np.subtract(Q, np.matmul(S,Q))
    Third_term = (beta/(2.0)) * pow((np.linalg.norm(Third_term_temp,ord='fro')),2)
    
    return First_term + Second_term + Third_term

iterations=10   #change this

for category in TRAINING_MATRICES.keys():
	if category in List_Category:	
		P=TRAINING_MATRICES[category][0]
		Q=TRAINING_MATRICES[category][1]
		R=TRAINING_MATRICES[category][2]
		S=TRAINING_MATRICES[category][3]
		r=TRAINING_MATRICES[category][4]
		I=TRAINING_MATRICES[category][5]

		# print "P : ",P.shape
		# print "Q : ",Q.shape
		# print "R : ",R.shape
		# print "S : ",S.shape
		# print "r : ",r.shape
		# print "I : ",I.shape
		print "#################################"
		print category

		R_Predicted = r + np.matmul(Q, P.transpose())   
		# print "R_Predicted : ",R_Predicted.shape

		for i in range(iterations): 

		    print "iteration no : ", str(i+1)

		    error_der_P = Error_derivative_wrt_P(I,R_Predicted,R,P,Q)
		    error_der_Q = Error_derivative_wrt_Q(I,R_Predicted,R,P,Q,S)  

		    P = np.subtract(P, np.multiply(l, error_der_P))        
		    Q = np.subtract(Q, np.multiply(l, error_der_Q))
		    
		    R_Predicted = r + np.matmul(Q, P.transpose())
		     
		    RMSE_train = np.sqrt(np.sum(np.square(np.subtract(np.multiply(I,R_Predicted),R)))/float(Rating_count[category]))
		  
		    New_Error = Training(I,R_Predicted,R,P,Q,S)
		    
		    print 'RMSE_train : ', str(RMSE_train)
		    print 'New_Error : ',str(New_Error)
		    print
		    
		    if i>0:    
		        if ((Old_Error-New_Error)<10):
		            break    
		    Old_Error = New_Error

# Testing
