import csv,numpy
"""Following program stores the count of rating for each actegory by each user as required by the papers
   Product.txt and Ratings.txt should be present in the directory"""

def write_reviews_matrix(infile, outfile, productfile):
	product_category_map={}
	cnt=0
	with open(productfile, 'r') as productf:
		for lines in productf.readlines():
			if cnt == 0:
				cnt+=1
				continue
			product_category_map[int(float(lines.split(',')[0]))]=int(float(lines.split(',')[1]))
	"""
	stores the count of ratings in each category by each user
	Since, unlike the user to user matrix, this can fit in memory, we dont need to use pickling
	"""
	"""The final Ratings matrix"""
	Ratings=numpy.zeros(shape=(100000,500) , dtype='int32')
	with open(outfile, 'w') as out:
		cnt=0
		writer=csv.writer(out)
		with open(infile, 'r') as inp:
			for ratings in inp.readlines():
				if cnt == 0:
					cnt+=1
					continue
				temp_list=ratings.split(',')
				Ratings[int(temp_list[0]),(product_category_map[int(temp_list[1])])]+=1
		numpy.savetxt(outfile, Ratings, delimiter=",")
	return Ratings

def get_product_category_map(productfile):
	product_category_map={}
	cnt=0
	with open(productfile, 'r') as productf:
		for lines in productf.readlines():
			if cnt == 0:
				cnt+=1
				continue
			product_category_map[int(float(lines.split(',')[0]))]=int(float(lines.split(',')[1]))
	return product_category_map

def get_product_category(product_id, product_map):
	if product_id not in product_map:
		return -1
	return product_map[product_id]


def get_matrix_row(row,Ratings):
	"""Returns the respective row of the matrix as per requirement"""
	if row>100000:
		return []
	return Ratings[row]

def main():
	Ratings = write_reviews_matrix("Rating.txt","ratings_output.csv","Products.txt" )
	print get_matrix_row(3,Ratings)
	Product_category_map=get_product_category_map("Products.txt")
	print get_product_category(2,Product_category_map)

"""Runtime : ~30 seconds"""

if __name__=="__main__":
	main()