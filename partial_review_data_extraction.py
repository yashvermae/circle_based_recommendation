import numpy,csv

"""
As per mohsin's requirement, extracts a single review per user_id 
instead of the multiple entries per user that were present
"""
def extract_single_entry_per_user(infile,outfile):
	with open(outfile,'wa') as out:
		writer=csv.writer(out)
		cnt=0
		with open(infile,'r') as inf:
			hashmap=numpy.zeros(100000)
			for lines in inf:
				if cnt==0:
					cnt=1
					continue
				temp_list=lines.strip('\n').split(',')
				#print temp_list
				#print int(temp_list[0])
				if hashmap[int(temp_list[0])]!=1:
					hashmap[int(temp_list[0])]=1
					writer.writerow(temp_list)

"""extracts only the reviews whose user_id < 1000
and whose product_id < 1000, so that a 1000*1000 
matrix constructed and used to train our model
without any memory error
"""
def extract_partial_data(infile, outfile):
	with open(outfile,'wa') as out:
		writer=csv.writer(out)
		cnt=0
		with open(infile, 'r') as inf:
			for lines in inf:
				if cnt==0:
					cnt+=1
					continue
				temp_list=lines.strip('\n').split(',')
				#print int(temp_list[0])
				#print int(temp_list[1])
				if (int(temp_list[0]) < 1000) and (int(temp_list[1]) < 1000):
					writer.writerow(temp_list)

def main():
	extract_single_entry_per_user("Rating.txt","single_out.csv")
	extract_partial_data("Rating.txt","partial_out.csv")

if __name__=="__main__":
	main()