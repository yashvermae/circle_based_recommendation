import pandas as pd
import re
import numpy as np
import pickle

d = 10           # dimension of latent space
lamda = 0.1      # regularization constant
#change this 
l = 0.0001       # learning rate
#change this
beta = 1         # Social information weight  

def Trustee_List(Trustee):
    if isinstance(Trustee, str): 
        Trustee_list=Trustee.split(",")
        return Trustee_list
    else:
        return []

# Error derivative wrt Q
def Error_derivative_wrt_Q(I, R_Predicted,R, P_,Q_, S_):
    First_term = np.matmul(np.multiply(I,(R_Predicted-R)), P_)
    Second_term = lamda * Q_
    Third_term = beta * np.subtract(Q_, np.matmul(S_,Q_))
    fourth_term = beta *  np.matmul(S_.transpose(),np.subtract(Q, np.matmul(S,Q))) 
    return First_term + Second_term + Third_term - fourth_term 

# Error derivative wrt P
def Error_derivative_wrt_P(I,R_Predicted,R,P,Q):
    First_term = np.matmul(np.multiply(I,(R_Predicted-R)), Q)
    Second_term = lamda * P
    return First_term + Second_term 

# Training 
def Training(I,R_Predicted,R,P,Q,S):
    First_term = np.sum(np.sum(np.multiply(R - np.multiply(R_Predicted,I), R - np.multiply(R_Predicted,I)))) / (2.0)
    Second_term = (lamda/(2.0)) *(np.linalg.norm(Q,ord='fro') + np.linalg.norm(P,ord='fro'))
    Third_term_temp = np.subtract(Q, np.matmul(S,Q))
    Third_term = (beta/(2.0)) * pow((np.linalg.norm(Third_term_temp,ord='fro')),2)
    
    return First_term + Second_term + Third_term

Product_df=pd.read_csv("Products.csv")  
User_Trust_Network_df=pd.read_csv("User_Trust_Network.csv", sep=':')

with open("Category.txt") as Category_file:
    Categories = [Category.strip() for Category in Category_file.readlines()]    

User_Trust_Network_df['Trustee'] = map(Trustee_List,User_Trust_Network_df['Trustee'])

#print len(User_Trust_Network_df)

R = np.empty([len(User_Trust_Network_df), len(Product_df)])
R_test = np.empty([len(User_Trust_Network_df), len(Product_df)])
I = np.empty([len(User_Trust_Network_df), len(Product_df)])
I_test = np.empty([len(User_Trust_Network_df), len(Product_df)])

# Create matrix S
S = np.zeros((len(User_Trust_Network_df), len(User_Trust_Network_df)))
for i in range(users):                   
    for user, friend in ([i] * len(User_Trust_Network_df['Trustee'][i]), User_Trust_Network_df['Trustee'][i]):
        S[user, friend] = 1
    
# Filling of S matrix   # change this

#Gradient Descent
np.random.seed(0)          
iterations=100   #change this

r = np.empty([len(User_Trust_Network_df), len(Products_df)])      
P = np.random.randn(len(Product_df), d)
Q = np.random.randn(len(User_Trust_Network_df), d) 

Training_df=pd.read_csv("Rating.csv") 
for user, rating_group in Training_df.groupby('User_id'):
    r[user][:] = np.mean(rating_group['Rating'])

Old_Error = 1000  #change this
R_Predicted = r + np.matmul(Q, P.transpose())   

for i in range(iterations): 
    error_der_P = Error_derivative_wrt_P(I,R_Predicted,R,P,Q)
    error_der_Q = Error_derivative_wrt_Q(I,R_Predicted,R,P,Q,S)  

    P = np.subtract(P, np.multiply(l, error_der_P))        
    Q = np.subtract(Q, np.multiply(l, error_der_Q))
    
    R_Predicted = r + np.matmul(Q, P.transpose())
     
    RMSE_train = np.sqrt(np.sum(np.square(np.subtract(np.multiply(I,R_Predicted),R)))/float(len(Trainning_data)))
    
    print 'RMSE_train : '
    print RMSE_train
    New_Error = Training(I,R_Predicted,R,P,Q,S)
    print 'New_Error : '
    print New_Error
    
    if i>0:    
        if ((Old_Error-New_Error)<1):
            break    
    Old_Error = New_Error