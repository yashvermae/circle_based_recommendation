import pandas as pd
import re
import numpy as np
from scipy.sparse import lil_matrix

d=10

Dict_Product_Category={}
No_of_items_in_category={}
TRAINING_MATRICES={}
Category_Circle={}

def Trustee_List(Trustee):
    return Trustee.split(",")

def Load_Dictionary(Product_df):
	for i in range(len(Product_df)):
		Dict_Product_Category[Product_df['Product_id'][i]]=Product_df['Category_names'][i]
		if Product_df['Category_names'][i] not in No_of_items_in_category:
			No_of_items_in_category[Product_df['Category_names'][i]]=0
		No_of_items_in_category[Product_df['Category_names'][i]]+=1

Product_df=pd.read_csv("Products.csv") 
Load_Dictionary(Product_df)

User_Trust_Network_df=pd.read_csv("User_Trust_Network.csv", sep=':') 
User_Trust_Network_df['Trustee'] = map(Trustee_List,User_Trust_Network_df['Trustee'])

items=len(Product_df)
users=len(User_Trust_Network_df)

with open("Category.txt") as Category_file:
	for Category in Category_file:
		if Category.strip() in No_of_items_in_category:
			P=lil_matrix(No_of_items_in_category[Category.strip()], d)
			Q=lil_matrix(users, d)
			R=lil_matrix(users, items)
			S=lil_matrix(users,users)
			r=lil_matrix(users,items)      
			TRAINING_MATRICES[Category.strip()]=[P,Q,R,S,r]

Training_df=pd.read_csv("Rating.csv")

# Creating Matrix S as Category_Circle (Varient a)
for user, rating_group in Training_df.groupby('User_id'):
    for rating_entry in rating_group['Product_id']:
    	if Dict_Product_Category[rating_entry] not in Category_Circle:
    		Category_Circle[Dict_Product_Category[rating_entry]]=dict()
    	if user not in Category_Circle[Dict_Product_Category[rating_entry]]:
    		Category_Circle[Dict_Product_Category[rating_entry]][user]=float(0)
    	Category_Circle[Dict_Product_Category[rating_entry]][user]+=1

for Category in Category_Circle:
	print Category
	Category_weight=0
	for user in Category_Circle[Category]:
		Category_weight+=Category_Circle[Category][user]
	for user in Category_Circle[Category]:
		Category_Circle[Category][user]/=float(Category_weight)
		print user,Category_Circle[Category][user]
	print "**************************************************************"

# print "hi"